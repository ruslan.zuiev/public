
#Пользователь вводит свое имя и возраст вывести строку в формате - “Hello {username} your age is {age}”, заменить текст в фигурных скобках на значения введенные пользователем.
name = input("Please input your name ")
age = input("Please input your age ")
print("Hello", name, "your age is", age)

#Пользователь вводит число, вывести его в 132 степени и Показать его остаток от деления на 3. Вывод должен быть в одну строку с пояснениями.
num1 = input("Plese input a number ")
val1 = int(num1)**123
val2 = int(val1) %3
print(num1, "powered by 123 equals", val1, "\nReminder of division of result by 3 equals", val2)

#Пользователь вводит 2 числа вывести каждую математическую операцию для этих чисел. Каждая новая операция должна быть выведена с новой строки с  пояснением.
num3 = input("Please input the first number ")
num4 = input("Please input the second number ")
add = int(num3) + int(num4)
mul = int(num3) * int(num4)
div = int(num3) / int(num4)
mod = int(num3) % int(num4)
exp = int(num3) ** int(num4)
flo = int(num3) // int(num4)
print(num3, 'plus', num4, 'equals', add)
print(num3, 'multiplied by', num4, 'equals', mul)
print(num3, 'divided by', num4, 'equals', div)
print('Reminder of division of', num3, 'by', num4, 'equals', mod)
print(num3, 'powered by', num4, 'equals', exp)
print('Floor Division of', num3, 'by', num4, 'equals', flo)

#Пользователь вводит 3 числа, подставить и посчитать формулу: 2a - 8b / (a-b+c). Вывести результат.
num5 = input("Please input the first number ")
num6 = input("Please input the second number ")
num7 = input("Please input the third number ")
val3 = 2 * int(num5) - 8 * int(num6) / (int(num5) - int(num6) + int(num7))
print('2a - 8b / (a-b+c) equals', val3)

#Пользователь вводит строку и число, вывести повторение строки равное введенному числу. Вывод должен быть в одну строку.
string1 = input('Please input a string ')
num7 = input('Please input a number ')
print(string1 * int(num7))

#Даны числа 125 и 437 вывести их остатки от деления на 2, 3, 10, 22 с пояснениями.
num8 = 125
num9 = 437
delimims = [2, 3, 10, 22]
for i in delimims:
  val4 = num8 % i
  val5 = num9 % i
  print('Reminder of division of', num8, 'by', i, 'equals', val4)
  print('Reminder of division of', num9, 'by', i, 'equals', val5)
#поленился делать без цикла, признаю ошибку

#Пользователь вводит 2 числа, вывести целую часть от деления одного на другое.
num10 = input("Please input the first number ")
num11 = input("Please input the second number ")
print(int(num10) // int(num11))

#Пользователь  вводит 3 строки. Вывести их в одну строку разделенные пробелом. В этой задаче метод print может принимать только 
string2 = input("Please input the first string ")
string3 = input("Please input the second string ")
string4 = input("Please input the third string ")
print(string2, string3, string4, sep=' ')

#Даны два числа first = 15 second = 43, записать first в second, a second в first. Вывести
first = 15
second = 43
first, second = second, first
print('first = ', first)
print('second =', second)
